require 'pry'
class Fixnum
  
  NUMBER_NAMES = ["thousand", "million", "billion", "trillion"]
  ZERO = "zero"
  ONES = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
  TEN_TO_NINETEEN = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"]
  TENS = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]
  
  def in_words
    return ZERO if self == 0
    groups = group_by_threes(self)
    translated_groups = groups.reverse.map.with_index { |str, i| humanify(str.to_i, i) }.reverse
    translated_groups.reject { |g| g.nil? }.join(" ")
  end
  
  # private
  
  def group_by_threes(number)
    num_str = number.to_s
    skip_first_n = num_str.length % 3
    groups = num_str[skip_first_n..-1].scan(/.{3}/)
    groups.unshift(num_str[0...skip_first_n]) if skip_first_n > 0
    groups
  end
  
  def humanify(num, exp)
    res = []
    # hundreds place
    if num >= 100
      n = num / 100
      res << "#{ONES[n-1]} hundred"
    end
    # tens and ones place
    trailing_digits = num % 100
    if trailing_digits != 0
      if trailing_digits >= 10 && trailing_digits < 20
        n = trailing_digits % 10
        res << "#{TEN_TO_NINETEEN[n]}"
      else
        tens = trailing_digits / 10
        ones = trailing_digits % 10
        trailing_digits_name = []      
        trailing_digits_name << "#{TENS[tens-2]}" if tens >= 2
        trailing_digits_name << "#{ONES[ones-1]}" if ones > 0
        res << trailing_digits_name.join(" ")
      end
    end
    # suffix
    res << NUMBER_NAMES[exp-1] if exp > 0 && num > 0
    res.empty? ? nil : res.join(" ")
  end
  
end